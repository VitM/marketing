### Terminus- banner ad targeting people who work at specific companies we designate
Read before submitting: ads take up to 3 days for temrinus to review. Design and copy creation takes at least a week. 

Details Needed:
* Campaign Start Date:
* End Date:
* Associated Campaigns:
* Industry:
* Sector:
* Targeted company/ companies:
* Titles targeted (default is IT management):
* Spend:
* Goal:
* Call to action:
* Page ad links to once clicked:
* desired ad style/ size (see below)
* Note: campaigns ad targeting end when opportinity created


Design Checklist:
* [ ] Artwork created
* [ ] Copy created
* 

Ad sizes:
* mobile: 320 x 50
* desktop: 
  *  300x600
  *  160x600
  *  300x250
  *  728x90
